// ---- CONDICIONALES ----
function imprimirProfesion(persona){
    console.log(`${persona.nombre}`)
    if(persona.informatico){
        console.log(`es informático`)
    }else{
        console.log(`no es informático`)
    }
}

function imprimirSiEsMayorEdad(persona){
    console.log(`${persona.nombre} es`)
    if (persona.edad < 18){
        console.log(`menor de edad`)
    }else{
        console.log(`mayor de edad`)
    }
}

var adri = {
    nombre: 'Adri',
    apellido: 'Antañón',
    edad: 26,
    informatico: true,
    administrativo: false,
    peso: 81
}

imprimirProfesion(adri)

//  ---- ARROW FUNCTIONS ----
// Proceso de transformación

const MAYORIA_DE_EDAD = 18

/*  Variable que es igual a una función anónima, es decir, para obtener el resultado deberemos invocar esMayordeEdad('AQUÍ VA EL OBJETO') y pasarle un objeto que contenga el parámetro edad para ser evaluado

------
const esMayordeEdad = function (persona){
    return persona.edad >= MAYORIA_DE_EDAD
}
------
 */

 /* Al hacerlo con una arrow function donde será obligatorio suprimir la palabra function, ya viene implícita en la declaración, si solo evaluamos un objeto/parámetro podemos obviar los paréntesis 

------
const esMayordeEdad = persona =>{
    return persona.edad >= MAYORIA_DE_EDAD
}
------
*/

/* Si la función lo único que hace es devolver algo podemos obviar el retur y borrar las llaves/corchetes/claudators ya que implícitamente va a devolver el valor de lo que hay a continuación */

const esMayordeEdad = persona => persona.edad >= MAYORIA_DE_EDAD

/* Si queremos desestructurar todavía más el objeto podemos hacer lo siguiente, lo que hacemos es que esperamos recibir un objeto que tenga edad como parámetro y es evaluada, a mi me queda más claro en el paso anterior PERO se puede hacer así si quieres ser más purista.

------
const esMayordeEdad = ({edad}) => edad >= MAYORIA_DE_EDAD
------

*/


function permitirAcceso(persona){
    console.log(`Solicita acceso ${persona.nombre}: `)

    if (!esMayordeEdad(persona)) console.log(`ACCESO DENEGADO `)
    else console.log('ACCESO PERMITIDO ')

    
}

permitirAcceso(adri)

// Reto: hacer una función con arrow function que te devuelva si eres true si eres menor y false si eres mayor

const MENOR_DE_EDAD = persona => !esMayordeEdad(persona)

MENOR_DE_EDAD(adri)


// Uso de FOR

const NUM_DIAS_ANYO = 365
const FLUCTUACIÓN_PESO = 0.200
const aumentarDePeso = persona => persona.peso += FLUCTUACIÓN_PESO
const adelgazar = persona => persona.peso -= FLUCTUACIÓN_PESO

console.log(`Peso de ${adri.nombre} en 2020: ${adri.peso} kg`)

for (let i=0; i<NUM_DIAS_ANYO; i++){
    let numAleatorio = Math.trunc(Math.random()*3)
    // console.log(numAleatorio)
    if (numAleatorio === 0){
        aumentarDePeso(adri)
    }else if(numAleatorio === 1){
        adelgazar(adri)
    }

}

console.log(`Peso en 2021: ${adri.peso.toFixed(2)} kg`)


// Uso de WHILE


const META = adri.peso - 3
var dias = 0
const comeMucho = () => Math.random() < 0.3
/*
Versión sin arrow function

 const comeMucho = function(){
    if (Math.random() < 0.3){
        return 0.3
    }

} */
const realizaDeporte = () => Math.random() < 0.5




while(META < adri.peso){
    if(comeMucho()){
        aumentarDePeso(adri)
    }
    if(realizaDeporte()){
        adelgazar(adri)
    }

    dias++
}

console.log(`${adri.nombre} ha tardado ${dias} días en llegar a su peso ideal`)


//  Uso del Do-while

var contador = 0
const LLUEVE = () => Math.random() < 0.25

do{
    contador++
}while(!LLUEVE())

if(contador === 1){
    console.log(`He ido ${contador} vez para ver si llovía, y llueve`) 
}else{
    console.log(`He ido un total de ${contador} veces para ver si llovía, y llueve`) 
}


// Uso del Switch

var signo = prompt("Cual es tu signo").toLowerCase();

switch(signo){
  case"acuario":
    console.log("Horoscopo para acuario")
    break
  case"piscis":
    console.log("Horoscopo para piscis")
    break
  case"aries":
    console.log("Horoscopo para aries")
    break
  default:
    console.log("No es un signo zodiacal valido")
    break
}