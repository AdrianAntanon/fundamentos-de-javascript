// Funciones como parámetros


class Persona {
    constructor(nombre, apellido, altura) {
        this.nombre = nombre
        this.apellido = apellido
        this.altura = altura
        
    }
    
    saludar() {
        console.log(`Hola, me llamo ${this.nombre} ${this.apellido} y mido ${this.altura} cm's`)
    }
    soyAlto_O_Bajo() {
        if (this.altura >= 180) {
            console.log(`${this.nombre} es una persona alta ya que mide ${this.altura}`)
        }
        else {
            console.log(`${this.nombre} no es una gran persona (de estatura), ya que solo mide ${this.altura}`)
        }
    }
}



class Desarrollador extends Persona {
    constructor(nombre, apellido, altura, programador) {
        super(nombre, apellido, altura)
        this.programador = programador
        
    }
    // Aquí está el parámetro que le paso, puede ser opcional y funcionará tanto si le paso uno o ninguno
    saludar(opcional) {

        let {nombre, apellido, programador} = this // Aquí lo que estoy haciendo es que las variables nombre, apellido y programador vienen de this, cuando las paso como parámetro no es necesario que pongo el this."variable" y me sirve con solo "variable"

        console.log(`Hola, me llamo ${this.nombre} ${this.apellido} y soy programador`)

        if (opcional){
            opcional(nombre,apellido, programador)

        }
        
    }
}
function responderSaludo(nombre, apellido, esProgramador){
    console.log(`Buenos días ${nombre} ${apellido}`)
    if (esProgramador){
        console.log('Y felicidades por ser programador')
    }
}

let adri = new Desarrollador('Adrián','Antañón', 188, true)





adri.saludar(responderSaludo)