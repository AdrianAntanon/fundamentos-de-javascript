//Debemos definir dos constantes partiendo la URL del API, en la primer constante debe ir la URL de llamada al API
//En la segunda constante debe ir el objeto a consultar junto con el campo de identificación, en este caso es un id

const API_URL = 'https://swapi.dev/api/'
const PEOPLE_URL = 'people/:id'

// Preguntamos el ID que quiere el personaje

// const ID_NUMBER = prompt('Introduce el ID del personaje que quieras, por favor')


//Después debemos declarar otras dos constantes, la primera es para generar la url completa del objeto a consultar
//La segunda es para generar las opciones que tendrá la función get de jQuery

// const SW_URL = `${API_URL}${PEOPLE_URL.replace(':id', ID_NUMBER)}`
const options = { crossDomain: true}

// Funciones que sirven para generar la llamada a la API, la primera es con las arrow functions ya que es una funcion donde solo se pasa un parámetro y nos devuelve un objeto (el console.log)
// La segunda es la que se encarga de la llamada con el $.get


//El asincronismo de Javascript devuelve los resultados de forma "desordenada" porque depende del tiempo de respuesta de cada uno de los request

// const onResponse =  character =>  console.log(`Hola, soy ${character.name} y mido ${character.height} centímetros`) es sustituída por la función que creo en el $.get

/* function getCharacter(id, callback){
    const SW_URL = `${API_URL}${PEOPLE_URL.replace(':id', id)}`

    const ERROR = () => console.log(`Error inesperado, no se pudo obtener el personaje ${id}`)

    // Esto es igual a lo de abajo --> $.get(SW_URL, options, callback).fail(ERROR)

    $
        .get(SW_URL, options, callback)
        .fail(ERROR)
    // El manejo de errores puede ser con .fail('Mensaje que queremos que le llegue al usuario')
}


getCharacter(1, function(character){
    console.log(`Hola, soy ${character.name} y mido ${character.height} centímetros`)
    getCharacter(2, function(character){
        console.log(`Hola, soy ${character.name} y mido ${character.height} centímetros`)
        getCharacter(3, function(character){
            console.log(`Hola, soy ${character.name} y mido ${character.height} centímetros`)
            getCharacter(100)
        })
    })
}) */


// Lo que vamos a hacer ahora es cambiar la función, para que reciba un segundo parámetro que será una función (de nombre callback)
// Al hacer esto, forzamos a que siga un orden, ya que no realizará el 2 hasta que finalice el 1, ya que realiza la busqueda del persona 1 y no pasa al siguiente hasta que se ha realizado la función anónima que le mando como segundo parámetro.

// Con esto lo que hacemos es forzar el orden, pasamos de pedir todo en paralelo(a la vez todas) a realizar una llamada en serie (1, 2, 3, 4...)




// PROMESAS

/* 
 Las promesas tienen tres estados:

    pending
    fullfilled
    rejected

Y se invocan de la siguiente forma:

new Promise( ( resolve, reject ) => {
  // --- llamado asíncrono
  if( todoOK ) {
     // -- se ejecutó el llamado exitosamente
     resolve()
  } else {
     // -- hubo un error en el llamado
     reject()
  }
} )
 */

//  Vamos a coger el ejemplo de el manejo de errores y callbacks

function getCharacter(id){
    return new Promise((resolve, reject) => {
        const SW_URL = `${API_URL}${PEOPLE_URL.replace(':id', id)}`
        $
            .get(SW_URL, options, function(data){
                resolve(data)
            })
            .fail(() => reject(id))
    })
        
}

function onError(id){
    console.log(`Error inesperado al obtener el personaje ${id}`)
}   

// Promesas encadenadas

/* getCharacter(10)
    .then(character =>{
        console.log(`Hola, soy ${character.name} y mido ${character.height} centímetros`)
        return getCharacter(2)
    })
    .then(character => {
        console.log(`Hola, soy ${character.name} y mido ${character.height} centímetros`)
        return getCharacter(3)
    })
    .then(character => {
        console.log(`Hola, soy ${character.name} y mido ${character.height} centímetros`)
       
    })
    .catch(onError)
 */


// Multiples promesas en paralelo

// let ids = [1,2,3,4,5,6,7,]

// let promesas = ids.map(id => getCharacter(id))

// Promise
//     .all(promesas)
//     .then(character => console.log(character))
//     .catch(onError)


// Async-await

async function getAllCharacters(){
    let ids = [1,2,3,4,5,6,7,]

    let promises = ids.map(id => getCharacter(id))
    
    try{
        let characters = await Promise.all(promises)
        console.log(characters)
    }catch(id){
        onError(id)
    }
}


getAllCharacters()





