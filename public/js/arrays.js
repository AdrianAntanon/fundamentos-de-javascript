// Creación de arrays e iteración

var adri ={
    nombre: 'Adri',
    altura: 1.88,
    cantidadVideojuegos: 200 
}

var joel = {
    nombre: 'Joel',
    altura: 1.78,
    cantidadVideojuegos: 500
}

var andrea = {
    nombre: 'Andrea',
    altura: 1.60,
    cantidadVideojuegos: 3
}

var arrayPersonas =[adri, joel, andrea]

for (let i=0;i<arrayPersonas.length;i++){
    var persona = arrayPersonas[i]
    console.log(`Persona: ${persona.nombre} y altura ${persona.altura} cm`)
}


// Filtrar elementos en un array
// Uso filter

const esAlta = persona => persona.altura >= 180
const esBaja = persona => persona.altura < 180

/* Que es básicamente esto
cons esAlta = function(persona){
    return persona.altura > 180
}
*/

var personasAltas = arrayPersonas.filter(esAlta)
var personasBajas = arrayPersonas.filter(esBaja)

console.log('Personas altas',personasAltas)
console.log('Personas bajas',personasBajas)


// Transformar un array
// Uso map

const pasarAlturaACms = persona => ({
     ...persona,
    altura:  persona.altura * 100
})

/* Sin simplificar tanto sería esto, está hecho así porque si no la copia se guarda en el mismo espacio de memoria y si modificas algo del segundo array también estás modicando el primero.
Con esto obligamos a que ocupe otro espacio en memoria y son independientes.

Si queremos comprobar si son DIFERENTES hacemos una comparación con ===, si devuelve true son iguales e ya.
const pasarAlturaACms = persona => {
    return{
        ...persona,
        altura:  persona.altura * 100
    } 
}
 */
let personasCms = arrayPersonas.map(pasarAlturaACms)

console.log('Array original con altura en x.xx',arrayPersonas)
console.log('Array modificado con la función map',personasCms)


// Reducir un array a un valor único



/* 
Versión sin usar la función reduce

let totalJuegos = 0

for(let i=0;i<arrayPersonas.length;i++){
    totalJuegos += arrayPersonas[i].cantidadVideojuegos 
}

console.log(`La cantidad de videojuegos que tienen entre los ${arrayPersonas.length} es de ${totalJuegos}`)

 */


/* Sin simplificar tanto
const reducer = (acumulacion, persona) => {
    return acumulacion + persona.cantidadVideojuegos
}
 */
const reducer = (acumulacion, persona) => acumulacion + persona.cantidadVideojuegos

 let totalJuegos = arrayPersonas.reduce(reducer, 0)

 console.log(`La cantidad de videojuegos que tienen entre los ${arrayPersonas.length} es de ${totalJuegos}`)