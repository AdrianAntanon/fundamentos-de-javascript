function imprimirNombreYEdad(persona){
    console.log(`Hola, me llamo ${persona.nombre} y tengo ${persona.edad} años`)
}

function cumpleanos(persona){
    return{
        ...persona,
        edad: persona.edad + 1
    }
}

var nombre = 'Adri'

var edad = 26
edad = '26 años'

console.log('Me llamo ' + nombre + ' y tengo ' + edad) 

var nombreEnMayus = nombre.toUpperCase()
var nombreEnMinus = nombre.toLowerCase()

console.log('El nombre en mayúsculas '+ nombreEnMayus +' y minúsculas ' + nombreEnMinus)

var peso = 81

var jugarAps4 = 3

var correr = 1

peso += jugarAps4

peso -= correr

var adri = {
    nombre: 'Adri',
    edad: 26
}

var joel = {
    nombre: 'Joel',
    edad: 20
}


