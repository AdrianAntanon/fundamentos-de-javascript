// Cómo funcionan las clases en JavaScript, funcionan mediante prototipos

class Persona {
    constructor(nombre, apellido, altura) {
        this.nombre = nombre
        this.apellido = apellido
        this.altura = altura
    }
    saludar() {
        console.log(`Hola, me llamo ${this.nombre} ${this.apellido} y mido ${this.altura} cm's`)

    }
    soyAlto_O_Bajo() {
        if (this.altura >= 180) {
            console.log(`${this.nombre} es una persona alta ya que mide ${this.altura}`)
        }
        else {
            console.log(`${this.nombre} no es una gran persona (de estatura), ya que solo mide ${this.altura}`)
        }
    }
}



let adri = new Persona('Adri','Antañón', 188)


adri.saludar()
adri.soyAlto_O_Bajo()


// Modificando un prototipo
// Recordar que es un prototipo es solo un objeto más, aquí solo ha incidido en lo importante que es que las funciones se escriban arriba del todo dentro de nuestro programa, ya que no es posible llamar a una función que todavía no ha sido creada

// El contexto de las funciones: quién es this
// this es window, es decir la ventana del navegador, por eso si intentas convertir las funciones en arrow functions te dice que this es undefined todo el rato.

// Cómo es la herencia en JS
// Los objetos en JavaScript son “contenedores” dinámicos de propiedades. Estos objetos poseen un enlace a un objeto prototipo. Cuando intentamos acceder a la propiedad de un objeto, la propiedad no sólo se busca en el propio objeto sino también en el prototipo del objeto, en el prototipo del prototipo, y así sucesivamente hasta que se encuentre una propiedad que coincida con el nombre o se alcance el final de la cadena de prototipos.


// TENGO QUE HACER EL CURSO DE ECMASCRIPT 6+, YA QUE MUCHO DE AQUÍ ESTÁ DESACTUALIZADO

/* function heredaDe(prototipoHijo, prototipoPadre){
    let fn = function(){}
    fn.prototype = prototipoPadre.prototype
    prototipoHijo.prototype = new fn
    prototipoHijo.prototype.constructor = prototipoHijo

} */

class Desarrollador extends Persona {
    constructor(nombre, apellido, altura) {
        super(nombre, apellido, altura)
        
    }
    saludar() {
        console.log(`Hola, me llamo ${this.nombre} ${this.apellido} y soy desarrollador`)
    }
}

let personaRandom = new Desarrollador('Adrián','Antañón')

personaRandom.saludar()


// Clases en JS, ECMAScript 2015, es igual que lo que he hecho en Java, lo puedo ver arriba, muy sencillo e intuitivo

/* Las clases de JavaScript son introducidas en el ECMAScript 2015 y son una mejora en la sintaxis sobre la herencia basada en prototipos de JavaScript.

La palabra clave extends se usa en declaraciones de clase o expresiones de clase para crear una clase que es hija de otra clase.

El método constructor es un método especial para crear e inicializar un objeto creado a partir de una clase. */