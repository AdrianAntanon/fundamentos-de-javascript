//Debemos definir dos constantes partiendo la URL del API, en la primer constante debe ir la URL de llamada al API
//En la segunda constante debe ir el objeto a consultar junto con el campo de identificación, en este caso es un id

const API_URL = 'https://swapi.dev/api/'
const PEOPLE_URL = 'people/:id'

// Preguntamos el ID que quiere el personaje

// const ID_NUMBER = prompt('Introduce el ID del personaje que quieras, por favor')


//Después debemos declarar otras dos constantes, la primera es para generar la url completa del objeto a consultar
//La segunda es para generar las opciones que tendrá la función get de jQuery

// const SW_URL = `${API_URL}${PEOPLE_URL.replace(':id', ID_NUMBER)}`
const options = { crossDomain: true}



const onResponse =  character =>  console.log(`Hola, soy ${character.name} y mido ${character.height} centímetros`)

function getCharacter(id){
    const SW_URL = `${API_URL}${PEOPLE_URL.replace(':id', id)}`
    $.get(SW_URL, options, onResponse)
}

// Funciones que sirven para generar la llamada a la API, la primera es con las arrow functions ya que es una funcion donde solo se pasa un parámetro y nos devuelve un objeto (el console.log)
// La segunda es la que se encarga de la llamada con el $.get


const MAX_CHARACTERS = 83

for(let i=1; i<=MAX_CHARACTERS;i++){
    getCharacter(i)
}

//El asincronismo de Javascript devuelve los resultados de forma "desordenada" porque depende del tiempo de respuesta de cada uno de los request


