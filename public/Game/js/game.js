const celeste = document.getElementById('celeste')
const violeta = document.getElementById('violeta')
const naranja = document.getElementById('naranja')
const verde = document.getElementById('verde')
const btnEmpezar = document.getElementById('btnEmpezar')
const LAST_LEVEL = 10

class Game {
    constructor() {
        this.initialize()
        this.generateSequence()
        setTimeout(this.nextLevel, 500)
    }

    initialize() {
        this.colorSelect = this.colorSelect.bind(this)
        this.nextLevel = this.nextLevel.bind(this)
        this.toogleBtnStart()
        this.level = 1
        this.colors = {
            celeste,
            violeta,
            naranja,
            verde
        }
    }

    toogleBtnStart(){
        if(btnEmpezar.classList.contains('hide')){
            btnEmpezar.classList.remove('hide')
        }else{
            btnEmpezar.classList.add('hide')
        }
    }

    generateSequence(){
        this.sequence = new Array(LAST_LEVEL).fill(0).map(n => Math.floor(Math.random() * 4))

       /*  function(n){
            return Math.floor(Math.random() * 4)
        } */
    }

    nextLevel(){
        this.numberHits = 0
        this.iluminateSequence()
        this.addEventClickGame()
    }

    transformNumberToColor(number){
        switch(number){
            case 0:
                return 'celeste'
            case 1:
                return 'violeta'
            case 2:
                return 'naranja'
            case 3:
                return 'verde'
        }
    }

    transformColorToNumber(color){
        switch(color){
            case 'celeste':
                return 0
            case 'violeta':
                return 1
            case 'naranja':
                return 2
            case 'verde':
                return 3
        }
    }
    


    iluminateSequence(){
        const SECOND = 1000
        for(let i=0; i<this.level;i++){
            const color = this.transformNumberToColor(this.sequence[i])

            setTimeout( () => this.iluminateColor(color), SECOND*i)
        }
    }

    iluminateColor(color){
        this.colors[color].classList.add('light')
        setTimeout(() => this.turnOffColor(color), 350)
    }

    turnOffColor(color){
        this.colors[color].classList.remove('light')
    }

    addEventClickGame(){
        this.colors.celeste.addEventListener('click', this.colorSelect)
        this.colors.verde.addEventListener('click', this.colorSelect)
        this.colors.naranja.addEventListener('click', this.colorSelect)
        this.colors.violeta.addEventListener('click', this.colorSelect)
    }

    eliminateEventsClick(){
        this.colors.celeste.removeEventListener('click', this.colorSelect)
        this.colors.verde.removeEventListener('click', this.colorSelect)
        this.colors.naranja.removeEventListener('click', this.colorSelect)
        this.colors.violeta.removeEventListener('click', this.colorSelect)
    }

    colorSelect(event){
        const colorName = event.target.dataset.color
        const colorNumber = this.transformColorToNumber(colorName)
        this.iluminateColor(colorName)
        if(colorNumber === this.sequence[this.numberHits]){
            this.numberHits++
            if(this.numberHits === this.level){
                this.level++
                this.eliminateEventsClick()
                if(this.level === (LAST_LEVEL + 1)){
                    this.winTheGame()
                } 
                else{
                    setTimeout(this.nextLevel, 1000)
                } 
            }
        }else{
            this.loseTheGame()
        }
    }


    winTheGame(){
        swal('Proyecto de Fundamentos de JS', 'Felicidades, has ganado!', 'success')
            .then(this.initialize.bind(this))
    }

    loseTheGame(){
        swal('Proyecto Fundamentos de JS', 'Has perdido, vuelve a intentarlo!', 'error')
            .then(() => {
                this.eliminateEventsClick()
                this.initialize()
            })
    }
    
   

}

function startGame() {
    let game = new Game()
}


